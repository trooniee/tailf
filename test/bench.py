import time

__all__ = ["bench_start", "bench_end", "bench_dump"]

table = {}
ts = {}


def bench_start(cap):
    global ts
    ts[cap] = time.time()


def bench_end(cap):
    global ts, table
    delta = time.time() - ts[cap]
    del ts[cap]
    table.setdefault(cap, 0.0)
    table[cap] += delta


def bench_dump():
    for key, value in table.items():
        print("%s: %.6f" % (key, value))
