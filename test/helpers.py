import asyncio
import functools


def nose_async(async_function):
    @functools.wraps(async_function)
    def wrapper(*args, **kwargs):
        loop = asyncio.new_event_loop()
        try:
            coro = async_function(*args, **kwargs)
            loop.run_until_complete(coro)
        finally:
            loop.close()

    return wrapper


def write_through(f, data):
    f.write(data)
    f.flush()
